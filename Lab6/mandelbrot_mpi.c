#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define WORK_REQUEST 0
#define WORK_DONE 1

#define BLOCK_WIDTH 224
#define BLOCK_HEIGHT 125
#define IMG_SIZE 28000

#define ROOT_NODE 0

//STATE variables
#define CENTERX -1.186340599860225
#define CENTERY -0.303652988644423
#define ZOOM 400
#define MAX_ITERATIONS 100

#define HUE_PER_ITERATION 5

//Final image
unsigned char* finalImage = NULL; 

void writeImage(unsigned char *img, int w, int h);

double When(){
	struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}


typedef struct wr_s{
	//ID of the node that requested the job
	int idx;
	//type
	int type;
} WorkerRequest;

//a struct for passing Mandelbrot-generation jobs to the other nodes
typedef struct mj_s{
	//id of the job in the master's jobList
	int idx;
	//position
	int px;
	int py;
	//width
	int w;
	//height
	int h;
} MandelbrotJob;

typedef struct mp_s{
	//ID of the node that computed the job
	int idx;
	//position of this piece
	int px;
	int py;
	//width
	int w;
	//height
	int h;
	//an array of pixel colors
	unsigned char img[BLOCK_WIDTH * BLOCK_HEIGHT * 3];
} MandelbrotPiece;

float iterationsToEscape(double x, double y, int maxIterations){
	double tempa;
	double a = 0.f;
	double b = 0.f;
	int i;
	for(i = 0; i < maxIterations; i++){
		tempa = a*a - b*b + x;
		b = 2*a*b + y;
		a = tempa;
		if(a*a+b*b > 64){
			return i;
			//return i - log(sqrt(a*a+b*b))/log(8);
		}
	}
	return -1;
}

int hue2rgb(float t){
	while (t>360) {
        t -= 360;
    }
    if (t < 60) return 255.*t/60.;
    if (t < 180) return 255;
    if (t < 240) return 255. * (4. - t/60.);
    return 0;
}

void printJob(MandelbrotJob _job){
	printf("%d: X: %d, Y: %d, W: %d, H: %d\n", _job.idx, _job.px, _job.py, _job.w, _job.h);
}

void main(int argc, char* argv[]){
	MPI_Status status;
	int nproc, iproc, i, row, col;
	double starttime, endtime;
	char hostname[255];
	
	//initialize MPI
	MPI_Init(&argc, &argv);
	//get the number of processors
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	//get your rank
	MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
	gethostname(hostname, 255);
	if(iproc == ROOT_NODE){
		printf("%s - %d: Hello from %d of %d\n", hostname, iproc, iproc, nproc);
	
		printf("Starting node %d of %d\n", iproc, nproc);
	}
	
	//---create the MPI structs-------------------------//
	//WorkerRequest
	WorkerRequest wr;
	MPI_Datatype MPI_Work_request;
	MPI_Datatype wrType[2] = {MPI_INT, MPI_INT};
	int wrBlocklen[2] = {1, 1};
	MPI_Aint wrDisp[2] = {
		offsetof(WorkerRequest, idx),
		offsetof(WorkerRequest, type)
	};
	MPI_Type_create_struct(2, wrBlocklen, wrDisp, wrType, &MPI_Work_request);
	MPI_Type_commit(&MPI_Work_request);
	
	//MandelbrotJob
	//MandelbrotJob mj;
	MPI_Datatype MPI_Mandelbrot_job;
	MPI_Datatype mjType[5] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT};
	int mjBlocklen[5] = {1,1,1,1,1};
	MPI_Aint mjDisp[5] = {
		offsetof(MandelbrotJob, idx),
		offsetof(MandelbrotJob, px),
		offsetof(MandelbrotJob, py),
		offsetof(MandelbrotJob, w),
		offsetof(MandelbrotJob, h)
	};
	MPI_Type_create_struct(5, mjBlocklen, mjDisp, mjType, &MPI_Mandelbrot_job);
	MPI_Type_commit(&MPI_Mandelbrot_job);
	
	//MandelbrotPiece
	//MandelbrotPiece mp;
	MPI_Datatype MPI_Mandelbrot_piece;
	MPI_Datatype mpType[6] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_UNSIGNED_CHAR};
	int mpBlocklen[6] = {1,1,1,1,1,BLOCK_WIDTH * BLOCK_HEIGHT * 3};
	MPI_Aint mpDisp[6] = {
		offsetof(MandelbrotPiece, idx),
		offsetof(MandelbrotPiece, px),
		offsetof(MandelbrotPiece, py),
		offsetof(MandelbrotPiece, w),
		offsetof(MandelbrotPiece, h),
		offsetof(MandelbrotPiece, img)
	};
	MPI_Type_create_struct(6, mpBlocklen, mpDisp, mpType, &MPI_Mandelbrot_piece);
	MPI_Type_commit(&MPI_Mandelbrot_piece);
	
	//---done creating structs--------------------------//
	
	//start the clock
	starttime = When();
	
	//if you're node 0
	if(iproc == ROOT_NODE){
		//create an array of structs and populate each with their respective centers and pixel dimensions
		int numJobsWidth = IMG_SIZE / BLOCK_WIDTH + (IMG_SIZE % BLOCK_WIDTH == 0?0:1);
		int numJobsHeight = IMG_SIZE / BLOCK_HEIGHT + (IMG_SIZE % BLOCK_HEIGHT == 0?0:1);
		//int numJobsHeight = numJobsWidth;
		//if the jobs aren't equal sizes, the last one will be smaller
		int leftover_w = IMG_SIZE % BLOCK_WIDTH;
		if(leftover_w == 0){
			leftover_w = BLOCK_WIDTH;
		}
		int leftover_h = IMG_SIZE % BLOCK_HEIGHT;
		if(leftover_h == 0){
			leftover_h = BLOCK_HEIGHT;
		}
		
		int numJobs = numJobsWidth * numJobsHeight;
		printf("%d: This is the number of jobs: %d\n", iproc, numJobs);
		//printf("%d: This is the size of one job: %d\n", iproc, sizeof(MandelbrotJob));
		
		int jobListSize = numJobs * sizeof(MandelbrotJob);
		//printf("This is the size of the job list: %d\n", jobListSize);
		MandelbrotJob *jobList = (MandelbrotJob*)malloc(jobListSize);
		//allocate memory for the final image
		if(finalImage) free(finalImage);
		long long size = (long long)IMG_SIZE * (long long)IMG_SIZE * 3;
		//printf("Malloc w %zu, h %zu,  %zu\n", IMG_SIZE, IMG_SIZE, size);
		finalImage = (unsigned char *)malloc(size);
		//printf("malloc returned %X\n",finalImage);
		
		long long j_idx;
		for(row = 0; row < numJobsHeight; row++){
			for(col = 0; col < numJobsWidth; col++){
				j_idx = row * numJobsWidth + col;
				jobList[j_idx].idx = j_idx;
				jobList[j_idx].px = col * BLOCK_WIDTH;
				jobList[j_idx].py = row * BLOCK_HEIGHT;
				jobList[j_idx].w = (col == numJobsWidth - 1? leftover_w : BLOCK_WIDTH);
				jobList[j_idx].h = (row == numJobsHeight - 1? leftover_h : BLOCK_HEIGHT);
				//printJob(jobList[j_idx]);
			}
		}
		
		int nextJob = 0;
		//make sure everyone has started up
		int started;
		for(i = 1; i < nproc; i++){
			MPI_Recv(&started, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			//printf("%d: Node %d checked in\n", iproc, started);
		}
		
		//broadcast the ready signal
		int signal = 1;
		//printf("%d: Broadcasting the ready signal\n", iproc);
		MPI_Bcast(&signal, 1, MPI_INT, ROOT_NODE, MPI_COMM_WORLD);
		
		MandelbrotPiece mp;
		MandelbrotJob tmpJob;	//a temporary job for sending to the worker nodes
		
		int numAlive = nproc - 1;
		int jobsLeft = numJobs;
		long long imgIdx;
		int pieceIdx;
		int pieceW, pieceH;
		int piecePX, piecePY;
		//while there are jobs left to compute
		while(jobsLeft > 0 || numAlive > 0){
			//wait for a request
			//printf("%d: Waiting for a request\n", iproc);
			MPI_Recv(&wr, 1, MPI_Work_request, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			
			//if it's a job request
			if(wr.type == WORK_REQUEST){
				//printf("%d: Received a work request from %d\n", iproc, wr.idx);
				//if we've already sent all the jobs, send a kill job
				if(nextJob >= numJobs){
					tmpJob.idx = -1;
					tmpJob.px = -1;
					tmpJob.py = -1;
					tmpJob.w = -1;
					tmpJob.h = -1;
					numAlive--;
					//printf("%d: Number alive: %d\n", iproc, numAlive);
				}
				else{
					tmpJob = jobList[nextJob];
					nextJob++;
				}
				//send the next job to the worker that requested it
				//printf("%d: About to send this job: ", iproc);
				//printJob(tmpJob);
				MPI_Send(&tmpJob, 1, MPI_Mandelbrot_job, wr.idx, 0, MPI_COMM_WORLD);
			}
			//if it's a completed job
			else if(wr.type == WORK_DONE){
				//printf("%d: Received a work DONE request from %d\n", iproc, wr.idx);
				//receive the rendered pixels from the node
				MPI_Recv(&mp, 1, MPI_Mandelbrot_piece, wr.idx, 0, MPI_COMM_WORLD, &status);
				pieceW = mp.w;
				pieceH = mp.h;
				piecePX = mp.px;
				piecePY = mp.py;
				//put the pixels into their correct position in the final image
				for(row = 0; row < pieceH; row++){
					for(col = 0; col < pieceW; col++){
						imgIdx = ((long long)(piecePY + row) * IMG_SIZE + (long long)(piecePX + col))*3;	//this index could get really big so we need to make sure its datatype can hold it. We also need to cast the pieces that will be used to calculate it
						pieceIdx = (row * pieceW + col) * 3;
						finalImage[imgIdx + 2] = mp.img[pieceIdx + 2];	//red
						finalImage[imgIdx + 1] = mp.img[pieceIdx + 1];	//green
						finalImage[imgIdx]     = mp.img[pieceIdx];		//blue
					}
				}
				jobsLeft--;
				//printf("%d: Jobs left: %d\n", iproc, jobsLeft);
			}
		}
		endtime = When();
		//write the image to disk
		//writeImage(finalImage, IMG_SIZE, IMG_SIZE);
	}
	//else
	else{
		//check in with the master node
		MPI_Send(&iproc, 1, MPI_INT, ROOT_NODE, 0, MPI_COMM_WORLD);
		//here's your running work request struct
		WorkerRequest wr;
		MandelbrotJob curJob;
		MandelbrotPiece curPiece;
		curPiece.idx = iproc;
		wr.idx = iproc;
		//wait for the ready signal
		int signal;
		MPI_Bcast(&signal, 1, MPI_INT, ROOT_NODE, MPI_COMM_WORLD);
		//while you're not done (you haven't gotten an empty job yet)
		//int count = 0;
		long long p_idx;
		unsigned char red, gre, blu;
		//double xs, ys;
		int px, py;
		int curWidth, curHeight;
		int img_width = IMG_SIZE;
		int img_height = IMG_SIZE;
		double centerX = CENTERX;
		double centerY = CENTERY;
		double zoom = ZOOM;
		float iterations, h;
		double xs[BLOCK_WIDTH];
		double ys[BLOCK_HEIGHT];
		while(1){
			//send a request for a job from the root node
			wr.type = WORK_REQUEST;
			//printf("%d: Here's my request I'm about to send: idx: %d, type: %d\n", iproc, wr.idx, wr.type);
			//printf("%d: Sending a work request to root\n", iproc);
			MPI_Send(&wr, 1, MPI_Work_request, ROOT_NODE, 0, MPI_COMM_WORLD);
			//printf("%d: Sent the request\n", iproc);
			//receive the job
			MPI_Recv(&curJob, 1, MPI_Mandelbrot_job, ROOT_NODE, 0, MPI_COMM_WORLD, &status);
			//if it's empty
			if(curJob.px == -1){
				//You've been killed
				//break out of the loop
				//printf("%d: Just got my kill job\n", iproc);
				break;
			}
			//count++;
			//printf("%d: here's the job I just got: idx: %d, px: %d, py: %d, w: %d, h: %d\n", iproc, curJob.idx, curJob.px, curJob.py, curJob.w, curJob.h);
			//copy the job data to the finished struct data
			curPiece.px = curJob.px;
			curPiece.py = curJob.py;
			curPiece.w = curJob.w;
			curPiece.h = curJob.h;
			//printf("%d: here's the piece I'm about to do: idx: %d, px: %d, py: %d, w: %d, h: %d\n", iproc, curJob.idx, curPiece.px, curPiece.py, curPiece.w, curPiece.h);
			
			px = curPiece.px;
			py = curPiece.py;
			
			curWidth = curPiece.w;
			curHeight = curPiece.h;
			
			//populate xs and yx
			for(row = 0; row < curHeight; row++){
				ys[row] = (double)(py + row - img_width/2) / zoom + centerY;
			}
			for(col = 0; col < curWidth; col++){
				xs[col] = (double)(px + col - img_width/2) / zoom + centerX;
			}
			
			//printf("%d: factor: %d\n", iproc, factor);
			//calculate the Mandelbrot set over the job's domain
			for(row = 0; row < curHeight; row++){
				//ys = (double)(py + row - img_height/2) / zoom + centerY;
				for(col = 0; col < curWidth; col++){
					p_idx = (row * curWidth + col) * 3;
					//printf("%d: my p_idx: %d  ", iproc, p_idx);
					//xs = (double)(px + col - img_width/2) / zoom + centerX;
					//printf("%d: %f, %f\n", iproc, xs, ys);
					iterations = iterationsToEscape(xs[col], ys[row], MAX_ITERATIONS);
					//printf("%d: iterations: %f\n", iproc, iterations);
					red = gre = blu = 0;
					if(iterations != -1){
						h = HUE_PER_ITERATION * iterations;
						red = hue2rgb(h + 120);
						gre = hue2rgb(h);
						blu = hue2rgb(h + 240);
					}
					//printf("%d: did I get to here?\n");
					curPiece.img[p_idx + 2] = (unsigned char)(red);
					curPiece.img[p_idx + 1] = (unsigned char)(gre);
					curPiece.img[p_idx] 	= (unsigned char)(blu);
					
					//FOR DEBUGGING
					/*int factor = 255 / (nproc - 1);
					curPiece.img[p_idx + 2]	= 255 - iproc * factor;		//red
					curPiece.img[p_idx + 1]	= 255 - iproc * factor;		//green
					curPiece.img[p_idx]		= 255;						//blue*/
					//printf("%d: did I get to here2?\n");
				}
			}
			//printf("%d: Finished my job\n");
			//send a request to send the job
			wr.type = WORK_DONE;
			MPI_Send(&wr, 1, MPI_Work_request, ROOT_NODE, 0, MPI_COMM_WORLD);
			//send the completed job
			MPI_Send(&curPiece, 1, MPI_Mandelbrot_piece, ROOT_NODE, 0, MPI_COMM_WORLD);
		}
		//printf("%d: I rendered %d jobs\n", iproc, count);
	}
	
	
	//if you're the root node, report the time
	if(iproc == ROOT_NODE){
		printf("%d: %f seconds\n", iproc, endtime - starttime);
	}
	
	//cleanup MPI
	MPI_Finalize();
}

void writeImage(unsigned char *img, int w, int h) {
    long long filesize = 54 + 3*(long long)w*(long long)h;
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    FILE *f;
    f = fopen("temp.bmp","wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    int i;
	for (i=0; i<h; i++) {
        long long offset = ((long long)w*(h-i-1)*3);
        fwrite(img+offset,3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }
    fclose(f);
}













