#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>
#include <math.h>

#define VECSIZE 8
#define ITERATIONS 10000
double When()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
int mylib_Reduce(void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm){
	//if(op != MPI_MAX)return -1;
	int c;
	MPI_Status status;
	//cast sendbuf and recvbuf to int arrays
	int *sbuf = (int*)sendbuf;
	int *rbuf = (int*)recvbuf;
	/*printf("sbuf:");
	for(c = 0; c < VECSIZE; c++){
		printf("%d, ", sbuf[c]);
	}
	printf("\nrbuf:");
	for(c = 0; c < VECSIZE; c++){
		printf("%d, ", rbuf[c]);
	}*/
	//store the contents of sbuf in rbuf
	for(c = 0; c < VECSIZE; c++){
		rbuf[c] = sbuf[c];
	}
	//get the number of nodes involved
	int nproc, iproc;
	MPI_Comm_size(comm, &nproc);
	float f_nproc = (float)nproc;
	//get my node rank
	MPI_Comm_rank(comm, &iproc);
	//get the number of dimensions of the hypercube
	int numdim = (int)ceil(log2(f_nproc));
	int notparticipating = 0, bitmask = 1, curdim, msg_dest, msg_src, i;
	int tmpValues[VECSIZE];
	for(curdim = 0; curdim < numdim; curdim++){
		if((iproc & notparticipating) == 0){
			if((iproc & bitmask) != 0){
				msg_dest = iproc ^ bitmask;
				//send your rbuf array to the next guy
				//printf("%d sending to %d\n", iproc, msg_dest);
				MPI_Send(rbuf, VECSIZE, MPI_INT, msg_dest, 0, comm);
			}
			else{
				msg_src = iproc ^ bitmask;
				if(msg_src < nproc){	//in case the number of processors isn't a power of 2
					//get the other guy's recvbuf and combine them into yours
					//printf("%d receiving from %d\n", iproc, msg_src);
					MPI_Recv(tmpValues, VECSIZE, MPI_INT, msg_src, 0, comm, &status);
					for(i = 0; i < VECSIZE; i++){
						//keep the max values
						if(tmpValues[i] > rbuf[i]){
							rbuf[i] = tmpValues[i];
						}
					}
				}
			}
		}
		notparticipating = notparticipating ^ bitmask;
		bitmask <<= 1;
	}
	
	return 0;
}

int mylib_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
	
	int c;
	MPI_Status status;
	//cast sendbuf and recvbuf to int arrays
	int *buf = (int*)buffer;
	
	//get the number of nodes involved
	int nproc, iproc;
	MPI_Comm_size(comm, &nproc);
	float f_nproc = (float)nproc;
	//get my node rank
	MPI_Comm_rank(comm, &iproc);
	//get the number of dimensions of the hypercube
	int numdim = (int)ceil(log2(f_nproc));
	int notparticipating = pow(2, numdim-1)-1;
	int bitmask = pow(2, numdim-1);
	int curdim, msg_dest, msg_src, i;
	int tmpValues[VECSIZE];
	for(curdim = 0; curdim < numdim; curdim++){
		if((iproc & notparticipating) == 0){
			if((iproc & bitmask) == 0){
				msg_dest = iproc ^ bitmask;
				//send your buf array to the next guy
				//printf("%d sending to %d\n", iproc, msg_dest);
				MPI_Send(buf, VECSIZE, MPI_INT, msg_dest, 0, comm);
			}
			else{
				msg_src = iproc ^ bitmask;
				if(msg_src < nproc){	//in case the number of processors isn't a power of 2
					//get the other guy's buf and combine them into yours
					//printf("%d receiving from %d\n", iproc, msg_src);
					MPI_Recv(tmpValues, VECSIZE, MPI_INT, msg_src, 0, comm, &status);
					//copy the values
					for(i = 0; i < VECSIZE; i++){
						buf[i] = tmpValues[i];
					}
				}
			}
		}
		notparticipating >>= 1;
		bitmask >>= 1;
	}
	
	return 0;
}

main(int argc, char *argv[])
{
        int iproc, nproc,i, iter;
        char host[255], message[55];
        MPI_Status status;

        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &iproc);

        gethostname(host,253);
        printf("I am proc %d of %d running on %s\n", iproc, nproc,host);
        // each process has an array of VECSIZE double: ain[VECSIZE]
        double ain[VECSIZE], aout[VECSIZE];
        int  ind[VECSIZE];
        struct {
            double val;
            int   rank;
        } in[VECSIZE], out[VECSIZE];
        int myrank, root = 0;

        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
        // Start time here
        srand(myrank+5);
        double start = When();
        for(iter = 0; iter < ITERATIONS; iter++) {
          for(i = 0; i < VECSIZE; i++) {
            ain[i] = rand();
//          printf("init proc %d [%d]=%f\n",myrank,i,ain[i]);
          }
          for (i=0; i<VECSIZE; ++i) {
            in[i].val = ain[i];
            in[i].rank = myrank;
          }
          MPI_Reduce( in, out, VECSIZE, MPI_DOUBLE_INT, MPI_MAXLOC, root, MPI_COMM_WORLD);
          // At this point, the answer resides on process root
          if (myrank == root) {
              /* read ranks out
               */
              for (i=0; i<VECSIZE; ++i) {
//                printf("root out[%d] = %f from %d\n",i,out[i].val,out[i].rank);
                  aout[i] = out[i].val;
                  ind[i] = out[i].rank;
              }
          }
          // Now broadcast this max vector to everyone else.
          MPI_Bcast(out, VECSIZE, MPI_DOUBLE_INT, root, MPI_COMM_WORLD);
          for(i = 0; i < VECSIZE; i++) {
//          printf("final proc %d [%d]=%f from %d\n",myrank,i,out[i].val,out[i].rank);
          }
        }
        MPI_Finalize();
        double end = When();
        if(myrank == root) {
          printf("Time %f\n",end-start);
        }
}
