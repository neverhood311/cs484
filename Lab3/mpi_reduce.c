#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>		//log2

#define VECSIZE 32768

double When();
int mylib_Reduce(void*, void*, int, MPI_Datatype, MPI_Op, int, MPI_Comm);
int mylib_Bcast(void*, int, MPI_Datatype, int, MPI_Comm);

double When(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int mylib_Reduce(void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm){
	//if(op != MPI_MAX)return -1;
	int c;
	MPI_Status status;
	//cast sendbuf and recvbuf to int arrays
	int *sbuf = (int*)sendbuf;
	int *rbuf = (int*)recvbuf;
	/*printf("sbuf:");
	for(c = 0; c < VECSIZE; c++){
		printf("%d, ", sbuf[c]);
	}
	printf("\nrbuf:");
	for(c = 0; c < VECSIZE; c++){
		printf("%d, ", rbuf[c]);
	}*/
	//store the contents of sbuf in rbuf
	for(c = 0; c < VECSIZE; c++){
		rbuf[c] = sbuf[c];
	}
	//get the number of nodes involved
	int nproc, iproc;
	MPI_Comm_size(comm, &nproc);
	float f_nproc = (float)nproc;
	//get my node rank
	MPI_Comm_rank(comm, &iproc);
	//get the number of dimensions of the hypercube
	int numdim = (int)ceil(log2(f_nproc));
	int notparticipating = 0, bitmask = 1, curdim, msg_dest, msg_src, i;
	int tmpValues[VECSIZE];
	for(curdim = 0; curdim < numdim; curdim++){
		if((iproc & notparticipating) == 0){
			if((iproc & bitmask) != 0){
				msg_dest = iproc ^ bitmask;
				//send your rbuf array to the next guy
				//printf("%d sending to %d\n", iproc, msg_dest);
				MPI_Send(rbuf, VECSIZE, MPI_INT, msg_dest, 0, comm);
			}
			else{
				msg_src = iproc ^ bitmask;
				if(msg_src < nproc){	//in case the number of processors isn't a power of 2
					//get the other guy's recvbuf and combine them into yours
					//printf("%d receiving from %d\n", iproc, msg_src);
					MPI_Recv(tmpValues, VECSIZE, MPI_INT, msg_src, 0, comm, &status);
					for(i = 0; i < VECSIZE; i++){
						//keep the max values
						if(tmpValues[i] > rbuf[i]){
							rbuf[i] = tmpValues[i];
						}
					}
				}
			}
		}
		notparticipating = notparticipating ^ bitmask;
		bitmask <<= 1;
	}
	
	return 0;
}

int mylib_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
	
	int c;
	MPI_Status status;
	//cast sendbuf and recvbuf to int arrays
	int *buf = (int*)buffer;
	
	//get the number of nodes involved
	int nproc, iproc;
	MPI_Comm_size(comm, &nproc);
	float f_nproc = (float)nproc;
	//get my node rank
	MPI_Comm_rank(comm, &iproc);
	//get the number of dimensions of the hypercube
	int numdim = (int)ceil(log2(f_nproc));
	int notparticipating = pow(2, numdim-1)-1;
	int bitmask = pow(2, numdim-1);
	int curdim, msg_dest, msg_src, i;
	int tmpValues[VECSIZE];
	for(curdim = 0; curdim < numdim; curdim++){
		if((iproc & notparticipating) == 0){
			if((iproc & bitmask) == 0){
				msg_dest = iproc ^ bitmask;
				//send your buf array to the next guy
				//printf("%d sending to %d\n", iproc, msg_dest);
				MPI_Send(buf, VECSIZE, MPI_INT, msg_dest, 0, comm);
			}
			else{
				msg_src = iproc ^ bitmask;
				if(msg_src < nproc){	//in case the number of processors isn't a power of 2
					//get the other guy's buf and combine them into yours
					//printf("%d receiving from %d\n", iproc, msg_src);
					MPI_Recv(tmpValues, VECSIZE, MPI_INT, msg_src, 0, comm, &status);
					//copy the values
					for(i = 0; i < VECSIZE; i++){
						buf[i] = tmpValues[i];
					}
				}
			}
		}
		notparticipating >>= 1;
		bitmask >>= 1;
	}
	
	return 0;
}



main(int argc, char *argv[]){
	int i;
	MPI_Status status;
	char host[255];
	int nproc, iproc;
	int root = 0;
	int mainvec[VECSIZE], outvec[VECSIZE]/*, finalvec[VECSIZE]*/;
	//initialize MPI
	MPI_Init(&argc, &argv);
	//get the number of processes
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	//get my process number
	MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
	//get my host name
	gethostname(host, 253);
	printf("I am processor %d of %d running on %s\n", iproc, nproc, host);
	
	//generate a bunch of random numbers
	srand(iproc + 3);
	for(i = 0; i < VECSIZE; i++){
		mainvec[i] = rand() % 200;
		//printf("%d: %d\n", iproc, mainvec[i]);
	}
	
	double start = When();
	
	//do a max reduce
	//you send out your values in mainvec and 
	// the reduced values will be put into outvec
	mylib_Reduce(mainvec, outvec, VECSIZE, MPI_INT, MPI_MAX, root, MPI_COMM_WORLD);
	//MPI_Reduce(mainvec, outvec, VECSIZE, MPI_INT, MPI_MAX, root, MPI_COMM_WORLD);
	
	
	if(iproc == root){
		//copy the max values into outvec
		/*for(i = 0; i < VECSIZE; i++){
			finalvec[i] = outvec[i];
			//printf("%d: Max %d: %d\n", iproc, i, outvec[i]);
		}*/
	}
	//broadcast these max values to everyone
	mylib_Bcast(outvec, VECSIZE, MPI_INT, root, MPI_COMM_WORLD);
	//MPI_Bcast(outvec, VECSIZE, MPI_INT, root, MPI_COMM_WORLD);
	
	//have each processor print out their final list
	/*
	printf("Proc %d final list:\n", iproc);
	for(i = 0; i < VECSIZE; i++){
		printf("%d, ", outvec[i]);
	}
	printf("\n");
	*/
	
	MPI_Finalize();
	
	double end = When();
	if(iproc == root){
		printf("Time: %f\n", end-start);
	}
}





