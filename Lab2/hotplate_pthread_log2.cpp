#include <stdio.h>		//printf
#include <stdlib.h>		//atoi
#include <math.h>		//fabs
#include <sys/time.h>	//timeval, gettimeofday
#include "pthread.h"	//pthread

#define GRID_SIZE 16384
#define GRID_SIZE_1 16383

#define MAXT 24

struct threadInfo{
	long tid;		//the thread's id
	int startrow;	//the index of the first row this thread is responsible for
	int numrows;	//the number of rows this thread is responsible for
	int nthreads;
};

typedef struct barrier_node {
        pthread_mutex_t count_lock;
        pthread_cond_t ok_to_proceed_up;
        pthread_cond_t ok_to_proceed_down;
        int count;
} mylib_barrier_t_internal;

typedef struct barrier_node mylob_logbarrier_t[MAXT];
int number_in_barrier = 0;
pthread_mutex_t logbarrier_count_lock;

void mylib_logbarrier (mylob_logbarrier_t b, int num_threads, int thread_id);
void mylib_init_barrier(mylob_logbarrier_t b);

mylob_logbarrier_t barr;


void mylib_init_barrier(mylob_logbarrier_t b)
{
        int i;
        for (i = 0; i < MAXT; i++) {
                b[i].count = 0;
                pthread_mutex_init(&(b[i].count_lock), NULL);
                pthread_cond_init(&(b[i].ok_to_proceed_up), NULL);
                pthread_cond_init(&(b[i].ok_to_proceed_down), NULL);
        }
        pthread_mutex_init(&logbarrier_count_lock, NULL);
}

void mylib_logbarrier (mylob_logbarrier_t b, int num_threads, int thread_id)
{
        int i, q, base, index;
        float *tmp;
        i = 2;
        base = 0;

        if (nproc == 1)
            return;

        pthread_mutex_lock(&logbarrier_count_lock);
        number_in_barrier++;
        if (number_in_barrier == nproc)
        {
                /* I am the last one in */
                /* swap the new value pointer with the old value pointer */
                tmp = oplate;
                oplate = iplate;
                iplate = tmp;
                /*
                fprintf(stderr,"%d: swapping pointers\n", thread_id);
                */

                /* set the keepgoing flag and let everybody go */
                keepgoing = 0;
                for (q = 0; q < nproc; q++)
                    keepgoing += lkeepgoing[q];
        }
        pthread_mutex_unlock(&logbarrier_count_lock);

        do {
                index = base + thread_id / i;
                if (thread_id % i == 0) {
                        pthread_mutex_lock(&(b[index].count_lock));
                        b[index].count ++;
                        while (b[index].count < 2)
                              pthread_cond_wait(&(b[index].ok_to_proceed_up),
                                        &(b[index].count_lock));
                        pthread_mutex_unlock(&(b[index].count_lock));
                }
                else {
                        pthread_mutex_lock(&(b[index].count_lock));
                        b[index].count ++;
                        if (b[index].count == 2)
                           pthread_cond_signal(&(b[index].ok_to_proceed_up));
/*
            while (b[index].count != 0)
*/
            while (
                               pthread_cond_wait(&(b[index].ok_to_proceed_down),
                                    &(b[index].count_lock)) != 0);
            pthread_mutex_unlock(&(b[index].count_lock));
            break;
                }
                base = base + num_threads/i;
                i = i * 2;
        } while (i <= num_threads);

        i = i / 2;

        for (; i > 1; i = i / 2)
        {
        base = base - num_threads/i;
                index = base + thread_id / i;
                pthread_mutex_lock(&(b[index].count_lock));
                b[index].count = 0;
                pthread_cond_signal(&(b[index].ok_to_proceed_down));
                pthread_mutex_unlock(&(b[index].count_lock));
        }
        pthread_mutex_lock(&logbarrier_count_lock);
        number_in_barrier--;
        pthread_mutex_unlock(&logbarrier_count_lock);
}

float grid1[GRID_SIZE][GRID_SIZE];
float grid2[GRID_SIZE][GRID_SIZE];
bool isFixed[GRID_SIZE][GRID_SIZE];
//pointers to each grid
float (*cur_grid)[GRID_SIZE] = grid2;
float (*pre_grid)[GRID_SIZE] = grid1;
float (*swap_grid)[GRID_SIZE];

//the number of simulation iterations
unsigned int iters = 0;

//the barrier
//pthread_barrier_t barr;
mylib_log_barrier_t barr_log;

//the global counters
int warmcells[MAXT];	//an array of warm cell counts, one element for each thread
int totalWarmCells;
bool steady[MAXT];		//an array of bools indicating whether a steady state has been reached by a particular thread
bool isSteady;

struct threadInfo threadInfoArray[MAXT];

double When(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

inline bool rowsAreSteady(int startRow, int endRow){
	float left, right, up, down, avg, cur, diff;
	int r, c;
	for(r = startRow; r <= endRow; r++){
		for(c = 1; c < GRID_SIZE_1; c++){
			if(!isFixed[r][c]){
				left = 		cur_grid[r][c-1];
				right = 	cur_grid[r][c+1];
				up = 		cur_grid[r-1][c];
				down = 		cur_grid[r+1][c];
				avg = (left + right + up + down)/4.f;
				cur = cur_grid[r][c];
				diff = fabs(cur - avg);
				if(diff > 0.1f){
					return false;
				}
			}
		}
	}
	return true;
}

//do the full simulation
void* simulate(void *arg){
	//cast arg as a threadInfo struct
	struct threadInfo *myInfo;
	long tid = (long)arg;
	myInfo = &threadInfoArray[tid];
	int nthreads = myInfo->nthreads;
	//printf("tid: %d\n startrow: %d\n numrows: %d\n nthreads: %d\n\n", myInfo->tid, myInfo->startrow, myInfo->numrows, nthreads);
	
	int startRow = myInfo->startrow;
	int numRows = myInfo->numrows;
	//get the index of the last row I'm responsible for
	int endRow = startRow + numRows - 1;
	
	int r, c, i;
	float left, right, up, down, avg, cur, diff;
	do{
		//if you're thread 0
		if(tid == 0){
			//swap the grids
			swap_grid = cur_grid;
			cur_grid = pre_grid;
			pre_grid = swap_grid;
			//increment the iteration counter
			iters++;
		}
		//BARRIER - wait for thread 0
		//pthread_barrier_wait(&barr);
		mylib_log_barrier(&barr_log, nthreads);
		
		//calculate the new state for your rows
		for(r = startRow; r <= endRow; r++){
			for(c = 1; c < GRID_SIZE_1; c++){
				if(!isFixed[r][c]){
					cur = 		pre_grid[r][c];
					left = 		pre_grid[r][c-1];
					right = 	pre_grid[r][c+1];
					up = 		pre_grid[r-1][c];
					down = 		pre_grid[r+1][c];
					cur_grid[r][c] = ((cur * 4.f) + up + down + left + right) / 8.f;
				}
			}
		}
		
		//set your warm cell count to 0
		warmcells[tid] = 0;
		//count the warm cells
		for(r = startRow; r <= endRow; r++){
			for(c = 1; c < GRID_SIZE_1; c++){
				if(!isFixed[r][c] && cur_grid[r][c] > 50.f){
					warmcells[tid]++;
				}
			}
		}
		
		//determine whether your section has reached a steady state
		steady[tid] = rowsAreSteady(startRow, endRow);
		//BARRIER - wait for everyone
		//pthread_barrier_wait(&barr);
		mylib_log_barrier(&barr_log, nthreads);
		
		//if you're thread 0
		if(tid == 0){
			//add the warm cell counts
			totalWarmCells = 0;
			for(i = 0; i < nthreads; i++){
				totalWarmCells += warmcells[i];
			}
			//subtract the total number of fixed warm cells
			//TODO
			//determine whether everyone has reached a steady state
			isSteady = true;
			for(i = 0; i < nthreads; i++){
				isSteady &= steady[i];
			}
			
		}
		//BARRIER - wait for thread 0 
		//pthread_barrier_wait(&barr);
		mylib_log_barrier(&barr_log, nthreads);
		//printf("tid %d: passed barrier 3\n", tid);
	}while(!isSteady);
	
}


int main(int argc, char*argv[]){
	
	double start = When();
	int nthreads = 1;
	if(argc > 1){
		nthreads = atoi(argv[1]);
	}
	
	//initialize the grids
	//everything else is 50
	int r, c;
	for(r = 1; r < GRID_SIZE_1; r++){
		for(c = 1; c < GRID_SIZE_1; c++){
			cur_grid[r][c] = 50.f;
			pre_grid[r][c] = 50.f;
			isFixed[r][c] = false;
		}
	}
	//bottom row set to 100 degrees
	for(c = 0; c < GRID_SIZE; c++){
		cur_grid[GRID_SIZE_1][c] = 100.f;
		pre_grid[GRID_SIZE_1][c] = 100.f;
		isFixed[GRID_SIZE_1][c] = true;
	}
	//top row, left and right columns set to 0 degrees
	for(c = 0; c < GRID_SIZE; c++){
		cur_grid[0][c] = 0.f;
		pre_grid[0][c] = 0.f;
		isFixed[0][c] = true;
	}
	for(r = 1; r < GRID_SIZE_1; r++){
		cur_grid[r][0] = 0.f;
		pre_grid[r][0] = 0.f;
		cur_grid[r][GRID_SIZE_1] = 0.f;
		pre_grid[r][GRID_SIZE_1] = 0.f;
		isFixed[r][0] = true;
		isFixed[r][GRID_SIZE_1] = true;
	}
	//row 400 columns 0 - 330 set to 100 degrees
	for(c = 0; c <= 330; c++){
		cur_grid[400][c] = 100.f;
		pre_grid[400][c] = 100.f;
		isFixed[400][c] = true;
	}
	//row 200 column 500 set to 100 degrees
	cur_grid[200][500] = 100.f;
	pre_grid[200][500] = 100.f;
	isFixed[200][500] = true;
	//every 20th row to 100 degrees
	for(r = 0; r < GRID_SIZE; r+=20){
		for(c = 0; c < GRID_SIZE; c++){
			cur_grid[r][c] = 100.f;
			pre_grid[r][c] = 100.f;
			isFixed[r][c] = true;
		}
	}
	//every 20th column to 0 degrees
	for(c = 0; c < GRID_SIZE; c+=20){
		for(r = 0; r < GRID_SIZE; r++){
			cur_grid[r][c] = 0.f;
			pre_grid[r][c] = 0.f;
			isFixed[r][c] = true;
		}
	}
	
	//initialize the barrier
	//pthread_barrier_init(&barr, NULL, nthreads);
	//mylib_init_log_barrier(&barr_log);
	
	//set all the steady states to false
	//split up the row indices among the threads
	int rowsPerThread = (GRID_SIZE - 2) / nthreads;
	int extraRows = (GRID_SIZE - 2) % nthreads;
	for(int i = 0; i < nthreads; i++){
		steady[i] = false;
		threadInfoArray[i].tid = i;
		threadInfoArray[i].numrows = rowsPerThread;
		threadInfoArray[i].nthreads = nthreads;
	}
	//because of integer division we need to adjust the number of rows per thread
	for(int i = nthreads - 1; extraRows > 0; i--, extraRows--){
		threadInfoArray[i].numrows++;
	}
	//set the starting row for each thread
	int startIDX = 1;
	for(int i = 0; i < nthreads; i++){
		threadInfoArray[i].startrow = startIDX;
		startIDX += threadInfoArray[i].numrows;
	}
	
	//create a bunch of pthreads
	pthread_t threadstructs[MAXT];
	long thread;
	
	//run the pthreads
	for(thread = 0; thread < nthreads; thread++){
		pthread_create(&threadstructs[thread], NULL, &simulate, (void *)thread);
	}
	
	//destroy the pthreads
	for(thread = 0; thread < nthreads; thread++){
		pthread_join(threadstructs[thread], NULL);
	}
	//destroy the barriers
	//pthread_barrier_destroy(&barr);
	
	double end = When();
	//report the number of iterations
	printf("Number of iterations: %d\n", iters);
	printf("Number of warm cells: %d\n", totalWarmCells);
	printf("Time taken: %f\n", end-start);
	
	return 0;
}
