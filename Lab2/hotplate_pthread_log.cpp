#include <stdio.h>		//printf
#include <stdlib.h>		//atoi
#include <math.h>		//fabs
#include <sys/time.h>	//timeval, gettimeofday
#include "pthread.h"	//pthread

#define GRID_SIZE 16384
#define GRID_SIZE_1 16384

#define MAXT 24

struct threadInfo{
	long tid;		//the thread's id
	int startrow;	//the index of the first row this thread is responsible for
	int numrows;	//the number of rows this thread is responsible for
	int nthreads;
};

//typedef struct mylib_log_barrier_t;

struct mylib_log_barrier_t{
	pthread_mutex_t count_lock;
	pthread_cond_t ok_to_proceed;
	int count;
	int parent;
	int child_left;
	int child_right;
	int tid;
};

void mylib_init_log_barrier(mylib_log_barrier_t *b, int tid){
	b->count = 0;
	//b->parent = parent;
	pthread_mutex_init(&(b->count_lock), NULL);
	pthread_cond_init(&(b->ok_to_proceed), NULL);
	b->tid = tid;
}

struct mylib_log_barrier_t log_barriers[MAXT];
int barrier_for_thread[MAXT];
int threadsPerBarrier = 2;

void mylib_init_barrier_tree(int nthreads){	
	int nextlayerstart = 0, layerCount = nthreads, i;
	int nextLayerCount, prelayerstart = -1000;
	//create nthreads number of mylib_log_barrier_t's
	for(i = 0; i < MAXT; i++){
		log_barriers[i].child_left = -1;
		log_barriers[i].child_right = -1;
		log_barriers[i].parent = -1;
		barrier_for_thread[i] = i / 2;
		//printf("barrier for %d is %d\n", i, barrier_for_thread[i]);
		mylib_init_log_barrier(&(log_barriers[i]), i);
	}
	
	if(nthreads == 1){
		threadsPerBarrier = 1;
		return;
	}
	//set the current number in the level to nthreads
	//while level number > 1
	while(layerCount > 1){
		//create (level number) / 2 new nodes
		nextLayerCount = (layerCount % 2 == 1)? layerCount / 2 + 1 : layerCount / 2;
		//if the level number is odd
		if(layerCount % 2 == 1){
			//add one more node
			//level number ++
			//layerCount++;
			//indicate that the tree is unbalanced
			//unbalanced = true;
		}
		//set the 1 or 2 children for each node
		for(i = 0; i < nextLayerCount; i++){
			log_barriers[nextlayerstart + i].child_left = prelayerstart + (i * 2);
			log_barriers[nextlayerstart + i].child_right = prelayerstart + (i * 2) + 1;
		}
		//level number /= 2
		layerCount /= 2;
		prelayerstart = nextlayerstart;
		nextlayerstart += nextLayerCount;
	}
	//if the tree is unbalanced
	/*if(unbalanced){
		//fix the tree
	}*/
	//set the parents for all nodes
	for(i = 0; i < nthreads; i++){
		if(log_barriers[i].child_left >= 0){
			log_barriers[log_barriers[i].child_left].parent = i;
		}
		if(log_barriers[i].child_right >= 0){
			log_barriers[log_barriers[i].child_right].parent = i;
		}
	}
}

void mylib_log_barrier(mylib_log_barrier_t *b, int num_threads){
	//printf("Entering barrier %d\n", b->tid);
	//printf("num_threads: %d\n", num_threads);
	//get a lock on the count
	pthread_mutex_lock(&(b->count_lock));
	//printf("locked the count\n");
	//increment the count
	b->count++;
	//printf("The count is now %d\n", b->count);
	//if you're the last one
	if(b->count == num_threads){
		//printf("I'm the last one\n");
		//if the barrier has a parent
		if(b->parent >=0){
			//proceed to the parent barrier
			b->count = 0;
			mylib_log_barrier(&(log_barriers[b->parent]), 2);
			pthread_cond_broadcast(&(b->ok_to_proceed));
		}
		else{
			//you're done
			//signal this barrier
			b->count = 0;
			pthread_cond_broadcast(&(b->ok_to_proceed));
		}
	}
	else{
		//printf("I'm NOT the last one\n");
		//wait for the ok_to_proceed condition
		while(pthread_cond_wait(&(b->ok_to_proceed), &(b->count_lock))!=0);
	}
	//printf("unlocking the count on barrier %d\n", b->tid);
	//unlock the count
	pthread_mutex_unlock(&(b->count_lock));
	//printf("unlocked the count on barrier %d\n", b->tid);
	//signal the barrier's children, if any
	if(b->child_left >= 0){
		pthread_cond_broadcast(&(log_barriers[b->child_left].ok_to_proceed));
	}
	if(b->child_right >= 0){
		pthread_cond_broadcast(&(log_barriers[b->child_right].ok_to_proceed));
	}
}


float grid1[GRID_SIZE][GRID_SIZE];
float grid2[GRID_SIZE][GRID_SIZE];
bool isFixed[GRID_SIZE][GRID_SIZE];
//pointers to each grid
float (*cur_grid)[GRID_SIZE] = grid2;
float (*pre_grid)[GRID_SIZE] = grid1;
float (*swap_grid)[GRID_SIZE];

//the number of simulation iterations
unsigned int iters = 0;

//the barrier
//pthread_barrier_t barr;
mylib_log_barrier_t barr_log;

//the global counters
int warmcells[MAXT];	//an array of warm cell counts, one element for each thread
int totalWarmCells;
bool steady[MAXT];		//an array of bools indicating whether a steady state has been reached by a particular thread
bool isSteady;

struct threadInfo threadInfoArray[MAXT];

double When(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

inline bool rowsAreSteady(int startRow, int endRow){
	float left, right, up, down, avg, cur, diff;
	int r, c;
	for(r = startRow; r <= endRow; r++){
		for(c = 1; c < GRID_SIZE_1; c++){
			if(!isFixed[r][c]){
				left = 		cur_grid[r][c-1];
				right = 	cur_grid[r][c+1];
				up = 		cur_grid[r-1][c];
				down = 		cur_grid[r+1][c];
				avg = (left + right + up + down)/4.f;
				cur = cur_grid[r][c];
				diff = fabs(cur - avg);
				if(diff > 0.1f){
					return false;
				}
			}
		}
	}
	return true;
}

//do the full simulation
void* simulate(void *arg){
	//cast arg as a threadInfo struct
	struct threadInfo *myInfo;
	long tid = (long)arg;
	myInfo = &threadInfoArray[tid];
	int nthreads = myInfo->nthreads;
	//printf("tid: %d\n startrow: %d\n numrows: %d\n nthreads: %d\n\n", myInfo->tid, myInfo->startrow, myInfo->numrows, nthreads);
	
	int startRow = myInfo->startrow;
	int numRows = myInfo->numrows;
	//get the index of the last row I'm responsible for
	int endRow = startRow + numRows - 1;
	
	int r, c, i;
	float left, right, up, down, avg, cur, diff;
	do{
		//if you're thread 0
		if(tid == 0){
			//swap the grids
			swap_grid = cur_grid;
			cur_grid = pre_grid;
			pre_grid = swap_grid;
			//increment the iteration counter
			iters++;
		}
		//BARRIER - wait for thread 0
		//pthread_barrier_wait(&barr);
		//printf("tid %d getting to barrier 1\n", tid);
		mylib_log_barrier(&(log_barriers[barrier_for_thread[tid]]), threadsPerBarrier);
		//printf("tid %d got past barrier 1\n", tid);
		
		//calculate the new state for your rows
		for(r = startRow; r <= endRow; r++){
			for(c = 1; c < GRID_SIZE_1; c++){
				if(!isFixed[r][c]){
					cur = 		pre_grid[r][c];
					left = 		pre_grid[r][c-1];
					right = 	pre_grid[r][c+1];
					up = 		pre_grid[r-1][c];
					down = 		pre_grid[r+1][c];
					cur_grid[r][c] = ((cur * 4.f) + up + down + left + right) / 8.f;
				}
			}
		}
		
		//set your warm cell count to 0
		warmcells[tid] = 0;
		//count the warm cells
		for(r = startRow; r <= endRow; r++){
			for(c = 1; c < GRID_SIZE_1; c++){
				if(!isFixed[r][c] && cur_grid[r][c] > 50.f){
					warmcells[tid]++;
				}
			}
		}
		
		//determine whether your section has reached a steady state
		steady[tid] = rowsAreSteady(startRow, endRow);
		//BARRIER - wait for everyone
		//pthread_barrier_wait(&barr);
		mylib_log_barrier(&(log_barriers[barrier_for_thread[tid]]), threadsPerBarrier);
		
		//if you're thread 0
		if(tid == 0){
			//add the warm cell counts
			totalWarmCells = 0;
			for(i = 0; i < nthreads; i++){
				totalWarmCells += warmcells[i];
			}
			//subtract the total number of fixed warm cells
			//TODO
			//determine whether everyone has reached a steady state
			isSteady = true;
			for(i = 0; i < nthreads; i++){
				isSteady &= steady[i];
			}
			
		}
		//BARRIER - wait for thread 0 
		//pthread_barrier_wait(&barr);
		mylib_log_barrier(&(log_barriers[barrier_for_thread[tid]]), threadsPerBarrier);
		//printf("tid %d: passed barrier 3\n", tid);
	}while(!isSteady);
	
}


int main(int argc, char*argv[]){
	
	double start = When();
	int nthreads = 1;
	if(argc > 1){
		nthreads = atoi(argv[1]);
	}
	
	//initialize the grids
	//everything else is 50
	int r, c;
	for(r = 1; r < GRID_SIZE_1; r++){
		for(c = 1; c < GRID_SIZE_1; c++){
			cur_grid[r][c] = 50.f;
			pre_grid[r][c] = 50.f;
			isFixed[r][c] = false;
		}
	}
	//bottom row set to 100 degrees
	for(c = 0; c < GRID_SIZE; c++){
		cur_grid[GRID_SIZE_1][c] = 100.f;
		pre_grid[GRID_SIZE_1][c] = 100.f;
		isFixed[GRID_SIZE_1][c] = true;
	}
	//top row, left and right columns set to 0 degrees
	for(c = 0; c < GRID_SIZE; c++){
		cur_grid[0][c] = 0.f;
		pre_grid[0][c] = 0.f;
		isFixed[0][c] = true;
	}
	for(r = 1; r < GRID_SIZE_1; r++){
		cur_grid[r][0] = 0.f;
		pre_grid[r][0] = 0.f;
		cur_grid[r][GRID_SIZE_1] = 0.f;
		pre_grid[r][GRID_SIZE_1] = 0.f;
		isFixed[r][0] = true;
		isFixed[r][GRID_SIZE_1] = true;
	}
	//row 400 columns 0 - 330 set to 100 degrees
	for(c = 0; c <= 330; c++){
		cur_grid[400][c] = 100.f;
		pre_grid[400][c] = 100.f;
		isFixed[400][c] = true;
	}
	//row 200 column 500 set to 100 degrees
	cur_grid[200][500] = 100.f;
	pre_grid[200][500] = 100.f;
	isFixed[200][500] = true;
	//every 20th row to 100 degrees
	for(r = 0; r < GRID_SIZE; r+=20){
		for(c = 0; c < GRID_SIZE; c++){
			cur_grid[r][c] = 100.f;
			pre_grid[r][c] = 100.f;
			isFixed[r][c] = true;
		}
	}
	//every 20th column to 0 degrees
	for(c = 0; c < GRID_SIZE; c+=20){
		for(r = 0; r < GRID_SIZE; r++){
			cur_grid[r][c] = 0.f;
			pre_grid[r][c] = 0.f;
			isFixed[r][c] = true;
		}
	}
	
	//printf("Initializing the barrier tree\n");
	//initialize the barrier
	//pthread_barrier_init(&barr, NULL, nthreads);
	mylib_init_barrier_tree(nthreads);
	//printf("Done initializing the barrier tree\n");
	//set all the steady states to false
	//split up the row indices among the threads
	int rowsPerThread = (GRID_SIZE - 2) / nthreads;
	int extraRows = (GRID_SIZE - 2) % nthreads;
	for(int i = 0; i < nthreads; i++){
		steady[i] = false;
		threadInfoArray[i].tid = i;
		threadInfoArray[i].numrows = rowsPerThread;
		threadInfoArray[i].nthreads = nthreads;
	}
	//because of integer division we need to adjust the number of rows per thread
	for(int i = nthreads - 1; extraRows > 0; i--, extraRows--){
		threadInfoArray[i].numrows++;
	}
	//set the starting row for each thread
	int startIDX = 1;
	for(int i = 0; i < nthreads; i++){
		threadInfoArray[i].startrow = startIDX;
		startIDX += threadInfoArray[i].numrows;
	}
	
	//create a bunch of pthreads
	pthread_t threadstructs[MAXT];
	long thread;
	
	//run the pthreads
	for(thread = 0; thread < nthreads; thread++){
		pthread_create(&threadstructs[thread], NULL, &simulate, (void *)thread);
	}
	
	//destroy the pthreads
	for(thread = 0; thread < nthreads; thread++){
		pthread_join(threadstructs[thread], NULL);
	}
	//destroy the barriers
	//pthread_barrier_destroy(&barr);
	
	double end = When();
	//report the number of iterations
	printf("Number of iterations: %d\n", iters);
	printf("Number of warm cells: %d\n", totalWarmCells);
	printf("Time taken: %f\n", end-start);
	
	return 0;
}
