#include <stdio.h>		//printf
#include <math.h>		//fabs
#include <sys/time.h>	//timeval, gettimeofday
#include "pthread.h"	//pthread

#define GRID_SIZE 16384
#define GRID_SIZE_1 16383

//I put these out here because I was getting a segmentation fault otherwise
float grid1[GRID_SIZE][GRID_SIZE];
float grid2[GRID_SIZE][GRID_SIZE];
//get pointers to each grid
float (*cur_grid)[GRID_SIZE] = grid2;
float (*pre_grid)[GRID_SIZE] = grid1;

double When(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

inline bool isFixed(int _r, int _c){
	if(_r == 400 && _c <= 330)return true;
	if(_r == 200 && _c == 500)return true;
	return false;
}

inline bool isSteady(){
	int r, c;
	//int rows_1 = GRID_SIZE-1;
	//int cols_1 = GRID_SIZE-1;
	float left, right, up, down, avg, cur, diff;
	//for each of the interior 4094 rows
	for(r = 1; r < GRID_SIZE_1; r++){
		//for each of the interior 4094 columns
		for(c = 1; c < GRID_SIZE_1; c++){
			if(!isFixed(r, c)){
				//get the average of the neighboring cells
				left = 		cur_grid[r][c-1];
				right = 	cur_grid[r][c+1];
				up = 		cur_grid[r-1][c];
				down = 		cur_grid[r+1][c];
				avg = (left + right + up + down)/4.f;
				//get the value of this cell
				cur = cur_grid[r][c];
				//get the absolute value of the difference between this cell and the neighboring cells
				diff = fabs(cur - avg);
				if(diff > 0.1f){
					return false;
				}
			}
		}
	}
	return true;
}



int main(){
	double start = When();
	
	float (*swap_grid)[GRID_SIZE];
	int r, c;
	
	//initialize the current simulation grid
	//initialize the previous simulation grid
	for(int r = 0; r < GRID_SIZE; r++){
		for(int c = 0; c < GRID_SIZE; c++){
			//top, left, right edges
			if(r == 0 || c == 0 || c == GRID_SIZE_1){
				cur_grid[r][c] = 0.f;
				pre_grid[r][c] = 0.f;
			}
			//bottom edge
			else if(r == GRID_SIZE_1){
				cur_grid[r][c] = 100.f;
				pre_grid[r][c] = 100.f;
			}
			//little row
			else if(r == 400 && c <= 330){
				cur_grid[r][c] = 100.f;
				pre_grid[r][c] = 100.f;
			}
			//single cell
			else if(r == 200 && c == 500){
				cur_grid[r][c] = 100.f;
				pre_grid[r][c] = 100.f;
			}
			//everything else
			else{
				cur_grid[r][c] = 50.f;
				pre_grid[r][c] = 50.f;
			}
		}
	}
	
	//set iteration count to zero
	unsigned long iters = 0;
	
	//some variables for use in the loops
	//int rows_1 = GRID_SIZE-1;
	//int cols_1 = GRID_SIZE-1;
	float cur, up, down, left, right;
	float avg, diff;
	do{
		//swap the current and previous simulation grids
		swap_grid = cur_grid;
		cur_grid = pre_grid;
		pre_grid = swap_grid;
		
		//for each of the interior 4094 rows
		for(r = 1; r < GRID_SIZE_1; r++){
			//for each of the interior 4094 columns
			for(c = 1; c < GRID_SIZE_1; c++){
				//if the cell is a fixed temperature
				if(isFixed(r, c)){
					//set the fixed temperature
					//don't need to cuz it's already set
				}
				else{
					//get the temperature for this cell
					cur = 		pre_grid[r][c];
					//get the temperatures for the four neighbors
					left = 		pre_grid[r][c-1];
					right = 	pre_grid[r][c+1];
					up = 		pre_grid[r-1][c];
					down = 		pre_grid[r+1][c];
					
					//set the new temperature to (this cell * 4 + the other cells)/8
					cur_grid[r][c] = ((cur * 4.f) + up + down + left + right)/8.f;
				}
			}
		}
		//increment the iteration counter
		iters++;
	//while the steady state has not been reached
	}while(!isSteady());
	//count how many cells have a temperature of more than 50 degrees
	int count = 0;
	for(int r = 1; r < GRID_SIZE_1; r++){
		for(int c = 1; c < GRID_SIZE_1; c++){
			if(!isFixed(r, c)){
				if(cur_grid[r][c] > 50.f){
					count++;
				}
			}
		}
	}
	
	double end = When();
	//report the number of iterations
	printf("Number of iterations: %d\n", iters);
	printf("Number of warm cells: %d\n", count);
	printf("Time taken: %f\n", end-start);
	
	return 0;
}