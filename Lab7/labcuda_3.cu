#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define BLOCKSIZE 1024
#define MAXIT 400
#define TOTROWS		(BLOCKSIZE*8)
#define TOTCOLS		(BLOCKSIZE*8)
#define NOTSETLOC       -1 // for cells that are not fixed
#define SETLOC	1
#define STEADY 0.1f

#define QMAX(x,y) (((x) > (y))? (x): (y))


int *lkeepgoing;
float *curgrid;
float *pregrid;
int *fixed;
float *tmp;
int ncols, nrows;

double When();
int Compute();


int main(int argc, char *argv[]){
	double t0, tottime;
	ncols = TOTCOLS;
	nrows = TOTROWS;
	int numIterations;

	//printf("Total rows: %d\nTotal columns: %d\n", nrows, ncols);
	//return 0;
	
	//allocate memory for the keepgoing grid
	cudaMalloc((void **) &lkeepgoing, nrows * ncols * sizeof(int));
	//allocate memory for the current grid
	cudaMalloc((void **) &curgrid, nrows * ncols * sizeof(float));
	//allocate memory for the previous grid
	cudaMalloc((void **) &pregrid, nrows * ncols * sizeof(float));
	//allocate memory for the grid of fixed cells
	cudaMalloc((void **) &fixed,  nrows * ncols * sizeof(int));
	//fprintf(stderr,"Memory allocated\n");

	t0 = When();
	/* Now proceed with the Jacobi algorithm */
	numIterations = Compute();
	tottime = When() - t0;
	fprintf(stderr,"Finished in %d iterations\n", numIterations);
	printf("Total time: %lf \n", tottime);

	return 0;
}

__global__ void InitArrays(float *curp, float *prep, int *fp, int *kp, int ncols){
	int i, curIdx, rowPos;
	float *preppos, *curppos;
	int *kppos, *fppos;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	
	// Each block gets a row, each thread will fill part of a row

	// Calculate the offset of the row
	blockOffset = blockIdx.x * ncols;
	// Calculate our offset into the row
	rowStartPos = threadIdx.x * (ncols/blockDim.x);
	// The number of cols per thread
	colsPerThread = ncols/blockDim.x;

	/*if(blockIdx.x == 0 && threadIdx.x == 0){
		printf("%d:%d - I have %d columns\n", blockIdx.x, threadIdx.x, colsPerThread);
	}*/
	
	curppos = curp + blockOffset+ rowStartPos;
	fppos = fp + blockOffset+ rowStartPos;
	preppos = prep + blockOffset+ rowStartPos;
	kppos = kp + blockOffset+ rowStartPos;

	for (i = 0; i < colsPerThread; i++) {
		fppos[i] = NOTSETLOC; // Not Fixed
		curppos[i] = 50;
		preppos[i] = 50;
		kppos[i] = 1; // Keep Going
	}
    //code to set the rest of the boundary and fixed positions
	//bottom row set to 0 degrees
	if(blockIdx.x == TOTROWS - 1){
		for(i = 0; i < colsPerThread; i++){
			curIdx = blockOffset + rowStartPos + i;
			fp[curIdx] = SETLOC;
			curp[curIdx] = 0;
			prep[curIdx] = 0;
			kp[curIdx] = 0;
		}
	}
	//top row set to 100 degrees
	if(blockIdx.x == 0){
		for(i = 0; i < colsPerThread; i++){
			curIdx = blockOffset + rowStartPos + i;
			fp[curIdx] = SETLOC;
			curp[curIdx] = 100;
			prep[curIdx] = 100;
			kp[curIdx] = 0;
		}
	}
	//left and right columns set to 100 degrees
	for(i = 0; i < colsPerThread; i++){
		rowPos = threadIdx.x * colsPerThread;
		if(rowPos == 0 || rowPos == ncols - 1){
			curIdx = blockOffset + rowStartPos + i;
			fp[curIdx] = SETLOC;
			curp[curIdx] = 100;
			prep[curIdx] = 100;
			kp[curIdx] = 0;
		}
	}
	
	//row 400, columns 0 through 330 are fixed at 100 degrees
	if(blockIdx.x == 400){
		int startFixed = 0;
		int endFixed = 330;
		for(i = 0; i < colsPerThread; i++){
			rowPos = threadIdx.x * colsPerThread;
			if(rowPos >= startFixed && rowPos <= endFixed){
				curIdx = blockOffset + rowStartPos + i;
				fp[curIdx] = SETLOC;
				curp[curIdx] = 100;
				prep[curIdx] = 100;
				kp[curIdx] = 0;
			}
		}
	}
	//row 200, column 500 is fixed at 100 degrees
	if(blockIdx.x == 200){
		rowPos = threadIdx.x * colsPerThread;
		for(i = 0; i < colsPerThread; i++){
			if(rowPos + i == 500){
				curIdx = blockOffset + rowStartPos + i;
				fp[curIdx] = SETLOC;
				curp[curIdx] = 100;
				prep[curIdx] = 100;
				kp[curIdx] = 0;
			}
		}
	}
}

/* Compute the 5 point stencil for my region */
__global__ void doCalc(float *curgrid, float *pregrid, int *fixed, int ncols){
	int i;
	int blockOffset;
	int rowStartPos;
	int colsPerThread;
	int curIdx;
	float up, down, left, right;
	
	//this is the first array index for the first thread in this block
	blockOffset = blockIdx.x * ncols;
	//this is the index of the first element I'm responsible for within the row that the block is responsible for
	rowStartPos = threadIdx.x * (ncols / blockDim.x);
	//the number of columns I'm responsible for = the number of columns that a block is responsible for / the number of threads in a block
	colsPerThread = ncols/blockDim.x;
	
	//for colsPerThread
	for(i = 0; i < colsPerThread; i++){
		curIdx = blockOffset + rowStartPos + i;
		//if I'm not a fixed cell
		if(fixed[curIdx] < 0){
			//get the north, south, east, and west neighbors from pregrid
			up = pregrid[curIdx - ncols];
			down = pregrid[curIdx + ncols];
			left = pregrid[curIdx - 1];
			right = pregrid[curIdx + 1];
			//the new value = ((sum of the neighbors) + (my value * 4)) / 8
			//store that new value in curgrid
			curgrid[curIdx] = ((up + down + left + right) + (pregrid[curIdx]) * 4.f) / 8.f;
		}
	}
}

// Calculate keepgoing array
__global__ void doCheck(float *curgrid, float *pregrid, int *fixed, int *lkeepgoing, int ncols){
	int i, blockOffset, rowStartPos, colsPerThread, curIdx;
	float up, down, left, right, diff;
	
	blockOffset = blockIdx.x * ncols;
	rowStartPos = threadIdx.x * (ncols / blockDim.x);
	colsPerThread = ncols/blockDim.x;
	
	for(i = 0; i < colsPerThread; i++){
		curIdx = blockOffset + rowStartPos + i;
		if(fixed[curIdx] < 0){
			up = curgrid[curIdx - ncols];
			down = curgrid[curIdx + ncols];
			left = curgrid[curIdx - 1];
			right = curgrid[curIdx + 1];
			diff = abs(curgrid[curIdx] - ((up + down + left + right) / 4.f));
			
			lkeepgoing[curIdx] = 1;
			if(diff < STEADY){
				//it's steady
				lkeepgoing[curIdx] = 0;
			}
		}
	}
}

__global__ void reduceSingle(int *idata, int *single, int nrows){
	int i, tid;
	//int rowStartPos;
	//int colsPerThread;
	extern __shared__ int parts[];	//parts has blocksize elements (1024)
	
	tid = threadIdx.x;
	//rowStartPos = tid * (nrows/blockDim.x);	//the index into idata where we start
	//colsPerThread = nrows / blockDim.x;	//the number of columns we're responsible for
	
	parts[tid] = 0;	//initialize my shared memory
	for(i = tid; i < nrows; i += blockDim.x){
		parts[tid] += idata[i];	//copy my part of the array into shared memory	
	}
	__syncthreads();
	//do the reduction
	for(unsigned int s = blockDim.x/2; s > 0; s >>=1){
		if(tid < s){
			parts[tid] += parts[tid + s];
		}
		__syncthreads();
	}
	
	if(tid == 0) {
		*single = parts[0];
	}
}
__global__ void reduceSum(int *idata, int *odata, unsigned int ncols){
	// Reduce rows to the first element in each row
	unsigned int i, tid;
	int blockOffset;
	
	extern __shared__ int parts[];	//parts has blocksize elements (1024)
	//this is the first array index for the first thread in this block
	blockOffset = blockIdx.x * ncols;
	
	tid = threadIdx.x;
	
	parts[tid] = 0;	//initialize my shared memory
	for(i = tid; i < ncols; i += blockDim.x){
		parts[tid] += idata[blockOffset + i];
	}
	__syncthreads();
	//do the reduction
	for(unsigned int s = blockDim.x/2; s > 0; s >>=1){
		if(tid < s){
			parts[tid] += parts[tid + s];
		}
		__syncthreads();
	}
	
	if(threadIdx.x == 0){
		odata[blockIdx.x] = parts[tid];
	}
	
}
	
int Compute(){
	int *keepgoing_single;
	int *keepgoing_sums;
	int keepgoing;
	int blocksize = BLOCKSIZE;
	int iteration;

	ncols = TOTCOLS;
	nrows = TOTROWS;

	//<<< number of blocks, number of threads per block>>>
	InitArrays<<< nrows, blocksize >>>(curgrid, pregrid, fixed, lkeepgoing, ncols);
	cudaMalloc((void **)&keepgoing_single, 1 * sizeof(int));
	keepgoing = 1;
	cudaMalloc((void **)&keepgoing_sums, nrows * sizeof(int));
 	//int *peek = (int *)malloc(nrows*sizeof(int));

	for (iteration = 0; (iteration < MAXIT) && keepgoing; iteration++)	{
		//perform the actual simulation
		doCalc<<< nrows, blocksize >>>(curgrid, pregrid, fixed, ncols);
		//see whether each cell has converged
		doCheck<<< nrows, blocksize >>>(curgrid, pregrid, fixed, lkeepgoing, ncols);
		//reduce all rows to a single number, one for each row
		reduceSum<<< nrows, blocksize, blocksize * sizeof(int)>>>(lkeepgoing, keepgoing_sums, ncols);
		/*cudaMemcpy(peek, keepgoing_sums, nrows*sizeof(int), cudaMemcpyDeviceToHost);
		fprintf(stderr, "after cudaMemcpy \n");
		int i;
 		for(i = 0; i < nrows; i++) {
			fprintf(stderr, "%d, ",peek[i]);
		}*/
		// Now we have the sum for each row in the first column, 
		//  reduce to one value
		reduceSingle<<<1, blocksize, blocksize*sizeof(int)>>>(keepgoing_sums, keepgoing_single, nrows);
		keepgoing = 0;
		//copy the reduced value into keepgoing
		cudaMemcpy(&keepgoing, keepgoing_single, 1 * sizeof(int), cudaMemcpyDeviceToHost);
		//fprintf(stderr, "keepgoing = %d\n", keepgoing);

		/* swap the new value pointer with the old value pointer */
		tmp = pregrid;
		pregrid = curgrid;
		curgrid = tmp;
	}
	//free(peek);
	//cleanup CUDA memory
	cudaFree(keepgoing_single);
	cudaFree(keepgoing_sums);
	return iteration;
	
}

/* Return the current time in seconds, using a double precision number.       */
double When(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}