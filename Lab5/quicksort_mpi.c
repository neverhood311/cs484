#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>		//log2

#define SIZE 10000000
#define NUM_SAMPLES 50

double When();


//the comparison function for qsort
int cmpfunc(const void*a, const void*b){
	return(*(int*)a - *(int*)b);
}

int values[SIZE];		//my copy of the array values
int sendVals[SIZE];	//a temporary array for sending values to another node
int recvVals[SIZE];	//a temporary array for receiving values from another node

void main(int argc, char *argv[]){
	MPI_Status status;
	//char host[255];
	int nproc, iproc, i, c;
	//int root = 0;
	MPI_Comm* comms;
	
	double endtime;
	MPI_Init(&argc, &argv);
	
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
	if(iproc == 0){
		printf("Sorting %d values on %d nodes\n", SIZE, nproc);
	}
	printf("Starting node %d of %d\n", iproc, nproc);
	
	//generate random values for my array
	int numValues = SIZE / nproc;
	if(iproc < SIZE % nproc){
		numValues++;
	}
	int seed = 5;
	//grab the command line argument if there is one
	if(argc == 2){
		seed = atoi(argv[1]);
	}
	//printf("%d: Seed is %d\n", iproc, seed);
	srand(iproc + seed);
	for(i = 0; i < numValues; i++){
		values[i] = rand() % 999 + 1;
	}
	
	double starttime = When();
	/*printf("Original list:\n");
	for(i = 0; i < numValues; i++){
		printf("%d: %f\n", iproc, values[i]);		
	}*/
	
	//Create the communicators
	int numdim = (int)ceil(log2((float)nproc));
	if(nproc == 1)numdim = 0;	//we don't need to split the communicator if there is only one node running
	comms = (MPI_Comm*)malloc(numdim * sizeof(MPI_Comm));
	int fullNum = pow(2, numdim);
	//get the world comm
	//groupSize = fullNum
	int groupSize = fullNum;
	//for the number of dimensions required
	int color;
	for(i = 0; i < numdim; i++){
		//your color = iproc / groupSize
		color = iproc / groupSize;
		//Split MPI_WORLD_COMM
		MPI_Comm_split(MPI_COMM_WORLD, color, iproc, &comms[i]);
		//cut the group size in half
		groupSize = groupSize >> 1;
	}
	
	int pVal = 0;
	int groupPVal = 0;
	int groupsize = nproc;
	int xormask = groupsize >> 1;
	int partnerProc, numSend, numRecv;
	int numSamples, sampleIter;
	int lastVal;
	//for the number of dimensions (comm groups)
	for(i = 0; i < numdim; i++){
		//find your pivot value for your data
		//take a sampling of 50 values (or the number of values you have, whichever is smaller)
		numSamples = (numValues < NUM_SAMPLES)? numValues : NUM_SAMPLES;
		sampleIter = numValues / numSamples;
		pVal = 0;
		for(c = 0; c < numSamples; c++){
			pVal += values[c * sampleIter];
		}
		pVal /= numSamples;
		//do an allreduce with comms[idx] to get the best pivot value
		//printf("my pval: %d\n", pVal);
		groupPVal = 0;
		MPI_Allreduce(&pVal, &groupPVal, 1, MPI_INT, MPI_SUM, comms[i]);
		
		groupPVal /= groupsize;
		//printf("%d: Group size: %d\n", iproc, groupsize);
		//printf("%d: Split value: %f\n", iproc, groupPVal);
		//figure out who your partner for this dimension is
		partnerProc = iproc ^ xormask;
		//if(partnerProc < nproc){
			//if your iproc is less than theirs
			lastVal = numValues - 1;
			numSend = 0;
			if(iproc < partnerProc){
				for(c = 0; c < numValues; c++){
					if(values[c] >= groupPVal){
						sendVals[numSend] = values[c];
						//values[c] = -1;
						//printf("%d: %d\n", iproc, sendVals[numSend]);
						numSend++;
						//fill in that spot with the last valid value
						values[c] = values[lastVal];
						values[lastVal] = -1;	//not really necessary
						c--;	//make sure this filled-in spot is considered in the next iteration
						lastVal--;	//move the end of the array up
						numValues--;	//decrement the number of array elements
					}
				}
			}
			//else
			else{
				for(c = 0; c < numValues; c++){
					if(values[c] < groupPVal){
						sendVals[numSend] = values[c];
						//printf("%d: %d\n", iproc, sendVals[numSend]);
						numSend++;
						//fill in that spot with the last value value
						values[c] = values[lastVal];
						values[lastVal] = -1;	//not really necessary
						c--;	//make sure this filled-in spot is considered in the next iteration
						lastVal--;	//move the end of the array up
						numValues--;	//decrement the number of array elements
					}
				}
			}
			//tell them how many values you're going to give them
			MPI_Send(&numSend, 1, MPI_INT, partnerProc, 0, MPI_COMM_WORLD);
			//ask them how many values they're going to send
			MPI_Recv(&numRecv, 1, MPI_INT, partnerProc, 0, MPI_COMM_WORLD, &status);
			
			//printf("%d: Sending %d values to proc %d\n", iproc, numSend, partnerProc);
			//give them all the values greater than or equal to the pivot
			//get values from them
			//printf("%d: Receiving %d values from proc %d\n", iproc, numRecv, partnerProc);
			MPI_Sendrecv(sendVals, numSend, MPI_INT, partnerProc, 0, recvVals, numRecv, MPI_INT, partnerProc, 0, MPI_COMM_WORLD, &status);
			
			
			//add the received values to your array
			for(c = 0; c < numRecv; c++){
				values[c+numValues] = recvVals[c];
			}
			numValues += numRecv;
		//}
		
		groupsize = groupsize >> 1;
		xormask = xormask >> 1;
	}
	
	//sort your values
	double startsort, endsort;
	startsort = When();
	qsort(values, numValues, sizeof(int), cmpfunc);
	endsort = When();
	printf("%d: Sorting %d values: %lf\n", iproc, numValues, endsort-startsort);
	
	
	
	//measure latency
	if(nproc > 1){
		for(i = 0; i < 8; i++){
			printf("latency test\n");
			int msg = 12;
			int msgret;
			if(iproc == 0){
				double startlatency, endlatency;
				//start the timer
				startlatency = When();
				//send a message to node 1
				MPI_Send(&msg, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
				//receive a message from node 1
				MPI_Recv(&msgret, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
				//stop the timer
				endlatency = When();
				//report the time difference
				printf("round-trip time: %lf\n", endlatency-startlatency);
				//printf("Sent %d, received %d\n", msg, msgret);
			}
			else if(iproc == 1){
				//recieve a message from node 0
				MPI_Recv(&msgret, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
				//msg = msgret + 1;
				//send a message to node 0
				MPI_Send(&msg, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
			}
		}
	}
	
	//measure bandwidth
	if(nproc > 1){
		printf("size of int: %d\n", sizeof(int));
		for(i = 0; i < 8; i++){
			printf("bandwidth test\n");
			if(iproc == 0){
				double startbandwidth, endbandwidth;
				//start the timer
				startbandwidth = When();
				//send one million values to node 1
				MPI_Send(&sendVals, 3000000, MPI_INT, 1, 0, MPI_COMM_WORLD);
				//receive one million values from node1
				MPI_Recv(&recvVals, 3000000, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);
				//stop the timer
				endbandwidth = When();
				//report the time difference
				printf("Round-trip time for bandwidth: %lf\n", endbandwidth - startbandwidth);
			}
			else if(iproc == 1){
				//receive one million values from node 0
				MPI_Recv(&recvVals, 3000000, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
				//send one million values back to node 0
				MPI_Send(&sendVals, 3000000, MPI_INT, 0, 0, MPI_COMM_WORLD);
			}
		}
	}
	
	
	/*printf("%d: Sorted list of %d values:\n", iproc, numValues);
	for(c = 0; c < numValues; c++){
		printf("%d: %d\n", iproc, values[c]);
	}*/
	
	
	endtime = When();	
	//if(iproc == 0){
		printf("%d: %lf seconds\n", iproc, endtime - starttime);
	//}
	
	//example of using quicksort
	/*float values[] = {88.f, 40.f, 7.f, 61.f, 87.f, 90.f, 29.f, 56.f, 100.f, 2.f};
	int n;
	qsort(values, 2, sizeof(float), cmpfunc);
	for(n = 0; n < 10; n++){
		printf("%f, ", values[n]);
	}
	printf("\n");*/
	
	MPI_Finalize();
}

/* Return the correct time in seconds, using a double precision number.       */
double When(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
