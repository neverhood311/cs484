/*
*	This version has an combination of all optimizations attempted:
*		parallelized array allocation
*		parallelized counting of warm cells
*		clever steady state checking
*		no conditionals in the main for loop
*/

#include <stdio.h>		//printf
#include <math.h>		//fabs
#include <sys/time.h>	//timeval, gettimeofday
#include <omp.h>		//omp_lock_t, omp_init_lock, omp_set_lock, omp_unset_lock, omp_destroy_lock

#define NUM_COLS 16384
#define NUM_ROWS 16384

//I put these out here because I was getting a segmentation fault otherwise
float grid1[NUM_ROWS][NUM_COLS];
float grid2[NUM_ROWS][NUM_COLS];
//get pointers to each grid
float (*cur_grid)[NUM_COLS] = grid2;
float (*pre_grid)[NUM_COLS] = grid1;

double When(){
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

/*inline bool isFixed(int _r, int _c){
	//if(_r == 400 && _c <= 330)return true;
	//if(_r == 200 && _c == 500)return true;
	//if(_r % 20 == 0)return true;
	//if(_c % 20 == 0)return true;
	return false;
}*/

int steady_r = 1;
int steady_c = 1;

inline bool isSteady(){
	int rows_1 = NUM_ROWS-1;
	int cols_1 = NUM_COLS-1;
	float left, right, up, down, avg, cur, diff;
	bool loop = false;
	//for each of the interior 4094 rows
	for(; steady_r < rows_1; steady_r++){
		if(loop)steady_c = 1;
		//for each of the interior 4094 columns
		for(; steady_c < cols_1; steady_c++){
			//if(!isFixed(steady_r, steady_c)){
				//get the average of the neighboring cells
				left = 		cur_grid[steady_r][steady_c-1];
				right = 	cur_grid[steady_r][steady_c+1];
				up = 		cur_grid[steady_r-1][steady_c];
				down = 		cur_grid[steady_r+1][steady_c];
				avg = (left + right + up + down)/4.f;
				//get the value of this cell
				cur = cur_grid[steady_r][steady_c];
				//get the absolute value of the difference between this cell and the neighboring cells
				diff = fabs(cur - avg);
				if(diff > 0.1f){
					return false;
				}
			//}
		}
		loop = true;
	}
	return true;
}



int main(){
	double start = When();
	
	float (*swap_grid)[NUM_COLS];
	int r, c;
	
	int rows_1 = NUM_ROWS-1;
	int cols_1 = NUM_COLS-1;
	//initialize the current simulation grid
	//initialize the previous simulation grid
	//top row
	for(c = 0; c < NUM_COLS; c++){
		cur_grid[0][c] = 0.f;
		pre_grid[0][c] = 0.f;
	}
	//left and right columns
	for(r = 1; r < NUM_ROWS; r++){
		cur_grid[r][0] = 0.f;
		cur_grid[r][cols_1] = 0.f;
		pre_grid[r][0] = 0.f;
		pre_grid[r][cols_1] = 0.f;
	}
	//bottom row
	for(c = 0; c < NUM_COLS; c++){
		cur_grid[rows_1][c] = 100.f;
		pre_grid[rows_1][c] = 100.f;
	}
	//interior (parallel)
	#pragma omp parallel for private(r, c)
	for(r = 1; r < rows_1; r++){
		for(c = 1; c < cols_1; c++){
			cur_grid[r][c] = 50.f;
			pre_grid[r][c] = 50.f;
		}
	}
	
	
	//set iteration count to zero
	unsigned long iters = 0;
	
	//some variables for use in the loops
	float cur, up, down, left, right;
	float avg, diff;
	do{
		//swap the current and previous simulation grids
		swap_grid = cur_grid;
		cur_grid = pre_grid;
		pre_grid = swap_grid;
		
		#pragma omp parallel for private(cur, left, right, up, down, r, c)
		//for each of the interior 4094 rows
		for(r = 1; r < rows_1; r++){
			//for each of the interior 4094 columns
			for(c = 1; c < cols_1; c++){
				//get the temperature for this cell
				cur = 		pre_grid[r][c];
				//get the temperatures for the four neighbors
				left = 		pre_grid[r][c-1];
				right = 	pre_grid[r][c+1];
				up = 		pre_grid[r-1][c];
				down = 		pre_grid[r+1][c];
				
				//set the new temperature to (this cell * 4 + the other cells)/8
				cur_grid[r][c] = ((cur * 4.f) + up + down + left + right)/8.f;
			}
		}
		
		//increment the iteration counter
		iters++;
	//while the steady state has not been reached
	}while(!isSteady());
	//count how many cells have a temperature of more than 50 degrees
	int count = 0;
	omp_lock_t writelock;
	omp_init_lock(&writelock);
	
	#pragma omp parallel for private(r, c)
	for(int r = 1; r < rows_1; r++){
		for(int c = 1; c < rows_1; c++){
			if(cur_grid[r][c] > 50.f){
				omp_set_lock(&writelock);
				count++;
				omp_unset_lock(&writelock);
			}
		}
	}
	omp_destroy_lock(&writelock);
	//subtract the number of fixed cells whose temperature is greater than 50 degrees
	//count -= 331;
	
	double end = When();
	//report the number of iterations
	printf("Number of iterations: %d\n", iters);
	printf("Number of warm cells: %d\n", count);
	printf("Time taken: %f\n", end-start);
	
	return 0;
}
