#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define GRIDSIZE 16384
#define GRIDSIZE_1 16383

inline float fabso(float f);
double When();


void main(int argc, char *argv[]){
	float **curGrid, **preGrid, **tmpGrid;
	float up, down, left, right, cur, avg;
	int count, i, d, r, c, done, alldone, numrows;
	int startrow, endrow;
	double starttime, endtime;
	int nproc, iproc;
	MPI_Status status;
	
	MPI_Init(&argc, &argv);
	starttime = When();
	
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
	//printf("Starting node %d of %d\n", iproc, nproc);
	
	//figure out my chunk size
	numrows = GRIDSIZE / nproc;
	//in case the rows don't divide evenly
	if(iproc < GRIDSIZE % nproc){
		numrows++;
	}
	//get my starting and ending rows (the ones I'll actually do the simulation on)
	startrow = 1;
	endrow = numrows;	//row 'end' is included in the calculations
	//allocate my arrays
	curGrid = (float **)malloc((numrows+2) * sizeof(float *));
	preGrid = (float **)malloc((numrows+2) * sizeof(float *));
	for(r = 0; r < numrows+2; r++){
		curGrid[r] = (float *)malloc(GRIDSIZE * sizeof(float));
		preGrid[r] = (float *)malloc(GRIDSIZE * sizeof(float));
	}
	
	
	//initialize my cells
	for(r = startrow; r <= endrow; r++){
		for(c = 1; c < GRIDSIZE_1; c++){
			curGrid[r][c] = preGrid[r][c] = 50.f;
		}
	}
	
	//initialize my boundaries
	//fil in the top row
	if(iproc == 0){
		startrow = 2;	//since we don't need to do calculations on the very first row
		for(c = 0; c < GRIDSIZE; c++){
			curGrid[1][c] = preGrid[1][c] = 0.f;
		}
	}
	//fill in the bottom row
	if(iproc == nproc - 1){
		endrow = numrows - 1;	//since we don't need to do calculations on the very last row
		for(c = 0; c < GRIDSIZE; c++){
			curGrid[endrow + 1][c] = preGrid[endrow + 1][c] = 100.f;
		}
	}
	
	//fill in the left and right columns
	for(r = startrow; r <= endrow; r++){
		curGrid[r][0] = preGrid[r][0] = 0.f;
		curGrid[r][GRIDSIZE_1] = preGrid[r][GRIDSIZE_1] = 0.f;
	}
	
	//printf("Processor %d: numrows: %d startrow: %d endrow: %d\n", iproc, numrows, startrow, endrow);
	
	//run the simulation
	alldone = 0;
	//while it's not all done
	for(count = 0; !alldone ; count++){
		if(iproc != 0){
			//trade previous boundary values with the north
			MPI_Send(&preGrid[startrow][0], GRIDSIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&preGrid[startrow-1][0], GRIDSIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
		}
		if(iproc != nproc - 1){
			//trade previous boundary values with the south
			MPI_Send(&preGrid[endrow][0], GRIDSIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&preGrid[endrow + 1][0], GRIDSIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
		}
		
		//do the calcuations
		for(r = startrow; r <= endrow; r++){
			for(c = 1; c < GRIDSIZE_1; c++){
				left 	= preGrid[r][c-1];
				right 	= preGrid[r][c+1];
				up 		= preGrid[r-1][c];
				down 	= preGrid[r+1][c];
				cur 	= preGrid[r][c];
				curGrid[r][c] = ((cur * 4.f) + up + down + left + right) / 8.f;
			}
		}
		
		
		if(iproc != 0){
			//trade new boundary values with the north
			MPI_Send(&curGrid[startrow][0], GRIDSIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&curGrid[startrow-1][0], GRIDSIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
		}
		if(iproc != nproc - 1){
			//trade new boundary values with the south
			MPI_Send(&curGrid[endrow][0], GRIDSIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD);
			MPI_Recv(&curGrid[endrow + 1][0], GRIDSIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
		}
		
		//determine whether we're done
		done = 1;
		for(r = startrow; r <= endrow; r++){
			for(c = 1; c < GRIDSIZE_1; c++){
				left 	= curGrid[r][c-1];
				right 	= curGrid[r][c+1];
				up 		= curGrid[r-1][c];
				down 	= curGrid[r+1][c];
				avg 	= (left + right + up + down)/4.f;
				cur 	= curGrid[r][c];
				if(fabso(avg - cur) > 0.1f){
					done = 0;
					break;
				}
			}
		}
		
		//do a reduce to see if everybody is done
		MPI_Allreduce(&done, &alldone, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
		
		//swap the pointers
		tmpGrid = curGrid;
		curGrid = preGrid;
		preGrid = tmpGrid;
	}
	endtime = When();	
	if(iproc == 0){
		printf("%d iterations in %lf seconds\n", count, endtime - starttime);
	}
	//printf("%d: %f\n", iproc, curGrid[5][5]);
	/*
	*/
	MPI_Finalize();
	
}

inline float fabso(float f){
    return (f > 0.0)? f : -f ;
}

/* Return the correct time in seconds, using a double precision number.       */
double When(){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}
